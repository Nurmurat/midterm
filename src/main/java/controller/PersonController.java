package controller;

import model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.PersonService;

@RestController
public class PersonController {

    private PersonService personService;

    public PersonController(PersonService PersonService) {
        this.personService = personService;
    }

    @GetMapping("/api/v2/users/{id}")
    public ResponseEntity<?> getPerson(@PathVariable Long personId) {
        return ResponseEntity.ok(personService.getById(personId));
    }

    @GetMapping("/api/v2/users")
    public ResponseEntity<?> getPeople() {
        return ResponseEntity.ok(personService.getAll());
    }

    @PostMapping("/api/v2/users")
    public ResponseEntity<?> savePerson(@RequestBody Person person) {
        return ResponseEntity.ok(personService.create(person));
    }

    @PutMapping("/api/v2/users")
    public ResponseEntity<?> updatePerson(@RequestBody Person person) {
        return ResponseEntity.ok(personService.create(person));
    }

    @DeleteMapping("/api/v2/users/{id}")
    public void deletePerson(@PathVariable Long personId) {
        personService.delete(personId);
    }
}
