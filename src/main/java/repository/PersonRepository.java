package repository;

import model.Person;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository extends CrudRepository<Person, Long> {


    @Query(value = "select * from person where id = ?", nativeQuery = true)
    Person getPerson();
    List<Person> findAll();
}