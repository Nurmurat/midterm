DROP TABLE IF EXISTS person;
create table person (
    id serial not null
    constraint person_pk unique,
    firstname varchar default 255,
    lastname varchar default 255,
    city varchar default 255,
    phone varchar default 50,
    telegram varchar default 50
);
INSERT INTO person (firstname,lastname,city, phone, telegram) VALUES
('Nurzhanat','Nurymkyzy','Astana','+77078264469', '@nurzh'),
('Aiganym','Sagidollinova','Semey','+77051182122', '@aiganymsag'),
('Kairat','Nurtas','Nur-Sultan','+77474491998', '@blabla');